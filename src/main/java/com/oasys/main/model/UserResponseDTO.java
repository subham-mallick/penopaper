
package com.oasys.main.model;

public class UserResponseDTO {
	private String responseId;
	private String response;

	public String getResponseId() {
		return responseId;
	}

	public void setResponseId(String responseId) {
		this.responseId = responseId;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return "UserResponseDTO [responseId=" + responseId + ", response=" + response + "]";
	}

}
