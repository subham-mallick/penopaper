
package com.oasys.main.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Question {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long questionId;
	private String questionType;
	private String questionText;
//	//private List<String> questionTag = null;
	private String questionDifficulty;
	private String option1;
	private String option2;
	private String option3;
	private String option4;
	
	@ElementCollection(targetClass = String.class)
	private List<String> correctOption = new ArrayList<>();
	
	
	private Integer marksAllocated;
	public Long getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	public String getQuestionText() {
		return questionText;
	}
	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}
	public String getQuestionDifficulty() {
		return questionDifficulty;
	}
	public void setQuestionDifficulty(String questionDifficulty) {
		this.questionDifficulty = questionDifficulty;
	}
	public String getOption1() {
		return option1;
	}
	public void setOption1(String option1) {
		this.option1 = option1;
	}
	public String getOption2() {
		return option2;
	}
	public void setOption2(String option2) {
		this.option2 = option2;
	}
	public String getOption3() {
		return option3;
	}
	public void setOption3(String option3) {
		this.option3 = option3;
	}
	public String getOption4() {
		return option4;
	}
	public void setOption4(String option4) {
		this.option4 = option4;
	}
	public List<String> getCorrectOption() {
		return correctOption;
	}
	public void setCorrectOption(List<String> correctOption) {
		this.correctOption = correctOption;
	}
	public Integer getMarksAllocated() {
		return marksAllocated;
	}
	public void setMarksAllocated(Integer marksAllocated) {
		this.marksAllocated = marksAllocated;
	}
	@Override
	public String toString() {
		return "Question [questionId=" + questionId + ", questionType=" + questionType + ", questionText="
				+ questionText + ", questionDifficulty=" + questionDifficulty + ", option1=" + option1 + ", option2="
				+ option2 + ", option3=" + option3 + ", option4=" + option4 + ", correctOption=" + correctOption
				+ ", marksAllocated=" + marksAllocated + "]";
	}

	
	

}
