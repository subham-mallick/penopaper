
package com.oasys.main.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "numberOfQuestionsAttended",
    "totalScore",
    "percentage"
})
public class Score {

    @JsonProperty("numberOfQuestionsAttended")
    private Integer numberOfQuestionsAttended;
    @JsonProperty("totalScore")
    private Integer totalScore;
    @JsonProperty("percentage")
    private Double percentage;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("numberOfQuestionsAttended")
    public Integer getNumberOfQuestionsAttended() {
        return numberOfQuestionsAttended;
    }

    @JsonProperty("numberOfQuestionsAttended")
    public void setNumberOfQuestionsAttended(Integer numberOfQuestionsAttended) {
        this.numberOfQuestionsAttended = numberOfQuestionsAttended;
    }

    @JsonProperty("totalScore")
    public Integer getTotalScore() {
        return totalScore;
    }

    @JsonProperty("totalScore")
    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    @JsonProperty("percentage")
    public Double getPercentage() {
        return percentage;
    }

    @JsonProperty("percentage")
    public void setPercentage(Double percentage) {
        this.percentage = percentage;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
