package com.oasys.main.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.oasys.main.model.Question;

public interface QuestionRepository extends JpaRepository<Question, Long> {
	
}