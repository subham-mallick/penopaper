package com.oasys.main.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.oasys.main.model.Question;
import com.oasys.main.service.QuestionService;

@RestController
public class TestController {
	@Autowired
	private QuestionService questionService;
	
	@GetMapping("/startTest")
	public List<Question> startTest() throws IOException {

		List<Question> questionList = questionService.getQuestionsFromDB();
		List<Question> questions = new ArrayList<>();
		if (null == questionList || questionList.isEmpty()) {
			return questions = questionService.getQuestions();
		} else
			return questionList;
		
	}

	/*
	 * @PostMapping("/submitTest") public Score submitForm(@RequestBody
	 * List<UserResponseDTO> userResponse) { userResponse.forEach(res ->
	 * System.out.print(res)); return questionService.verifyResponse(userResponse);
	 * }
	 */
	
	

}
